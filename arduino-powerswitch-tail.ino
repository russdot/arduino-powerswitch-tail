#define HIGH 1
#define LOW 0

#define OUT0 2
#define OUT1 3
#define OUT2 4
#define OUT3 5

#define IN0 6
#define IN1 7

#define NUM_PATTERNS 9


long motion_timeout_start_time = 0; // Time when motion stops, for determining timeout
int motion_timeout = 3000; // Milliseconds to wait until after motion stops
long motion_start_time = 0; // Time when motion is detected
long motion_duration = 0;
long pattern_start_time = 0; // Time when pattern is reset
int pattern_timeout = 5000; // Total time to run each pattern
long pattern_duration = 0;
int default_dt = 400; // Milliseconds to wait between pattern steps


boolean active = false;
boolean motion = false;
boolean motion_prev = false;
boolean sigdown = false;
boolean sigup = false;


void setup() {
  // Set output pins
  pinMode(OUT0, OUTPUT);
  pinMode(OUT1, OUTPUT);
  pinMode(OUT2, OUTPUT);
  pinMode(OUT3, OUTPUT);

  // Set input pins
  pinMode(IN0, INPUT);
  pinMode(IN1, INPUT);

  // Serial communication for monitoring
  Serial.begin(9600);
  Serial.println("Setup started");

  Serial.println("Test cycle");
  test_pattern();
  Serial.println("Test cycle complete");
  
  Serial.println("Setup complete");
}

bool detect_motion() {
  return digitalRead(IN0) == HIGH || digitalRead(IN1) == HIGH;
}

void setValues(int a, int b, int c, int d) {
  digitalWrite(OUT0, a);
  digitalWrite(OUT1, b);
  digitalWrite(OUT2, c);
  digitalWrite(OUT3, d);
}

void test_pattern() {
  setValues(1,0,0,0);
  delay(default_dt);
  setValues(0,1,0,0);
  delay(default_dt);
  setValues(0,0,1,0);
  delay(default_dt);
  setValues(0,0,0,1);
  delay(default_dt);
  setValues(0,0,0,0);
}


// All ON
void pattern_all_on() {
  setValues(1,1,1,1);
}

void pattern_startup(int dt) {
  pattern_duration = millis() - pattern_start_time;
  if (pattern_duration < dt) {
    setValues(0,0,0,1);
  }
  else if (pattern_duration < dt*1.2) {
    setValues(0,0,1,0);
  }
  else if (pattern_duration < dt*1.4) {
    setValues(0,1,0,1);
  }
  else if (pattern_duration < dt*1.6) {
    setValues(1,0,1,0);
  }
  else if (pattern_duration < dt*1.8) {
    setValues(0,1,1,1);
  }
  else if (pattern_duration < dt*2) {
    setValues(1,0,1,1);
  }
  else {
    setValues(1,1,1,1);
  }
}

void pattern_shutdown(int dt) {
  pattern_duration = millis() - pattern_start_time;
  
  if (pattern_duration < dt) {
    setValues(1,1,1,1);
  }
  else if (pattern_duration < dt*1.4) {
    setValues(1,1,0,1);
  }
  else if (pattern_duration < dt*1.8) {
    setValues(1,1,1,0);
  }
  else if (pattern_duration < dt*2.2) {
    setValues(1,0,0,1);
  }
  else if (pattern_duration < dt*2.6) {
    setValues(0,1,0,0);
  }
  else if (pattern_duration < dt*2) {
    setValues(1,0,0,0);
  }
  else {
    setValues(0,0,0,0);
  }
}

void pattern_twinkle_bright(int dt) {
  pattern_duration = millis() - pattern_start_time;
  
  if (pattern_duration < dt ) {
    setValues(0,1,1,1);
  }
  else if (pattern_duration < dt*2) {
    setValues(1,1,0,1);
  }
  else if (pattern_duration < dt*3) {
    setValues(1,0,1,1);
  }
  else if (pattern_duration < dt*4) {
    setValues(1,1,1,0);
  }
  else {
    pattern_start_time = millis();//repeat
  }
}

void pattern_twinkle_bright_repeat(int dt) {
  pattern_duration = millis() - pattern_start_time;
  
  if (pattern_duration < dt) {
    setValues(1,1,1,1);
  }
  else if (pattern_duration < dt*2) {
    setValues(0,1,1,1);
  }
  else if (pattern_duration < dt*3) {
    setValues(0,1,0,1);
  }
  else if (pattern_duration < dt*4) {
    setValues(1,1,0,1);
  }
  else if (pattern_duration < dt*5) {
    setValues(1,1,1,1);
  }
  else if (pattern_duration < dt*6) {
    setValues(1,1,1,0);
  }
  else if (pattern_duration < dt*7) {
    setValues(1,0,1,0);
  }
  else if (pattern_duration < dt*8) {
    setValues(1,0,1,1);
  }
  else {
    pattern_start_time = millis();//repeat
  }
}

void pattern_twinkle_dim_simple(int dt) {
  pattern_duration = millis() - pattern_start_time;
  
  if (pattern_duration < dt ) {
    setValues(1,0, 1,0);
  }
  else if (pattern_duration < dt*2) {
    setValues(0,1, 0,1);
  }
  else {
    pattern_start_time = millis();//repeat
  }
}

void pattern_twinkle_dim(int dt) {
  pattern_duration = millis() - pattern_start_time;
  
  if (pattern_duration < dt ) {
    setValues(1,0, 1,0);
  }
  else if (pattern_duration < dt*2) {
    setValues(0,0, 0,1);
  }
  else if (pattern_duration < dt*3) {
    setValues(0,1, 1,0);
  }
  else if (pattern_duration < dt*4) {
    setValues(1,0, 0,0);
  }
  else if (pattern_duration < dt*5) {
    setValues(0,1, 0,1);
  }
  else {
    pattern_start_time = millis();//repeat
  }
}

// Simple Alternating
void pattern_simple_alternate_dim(int dt) {
  pattern_duration = millis() - pattern_start_time;
  if (pattern_duration < dt ) {
    setValues(0,1,0,1);
  }
  else if (pattern_duration < dt*2) {
    setValues(1,0,1,0);
  }
  else {
    pattern_start_time = millis();//repeat
  }
}


void pattern_cycle_dim(int dt) {
  pattern_duration = millis() - pattern_start_time;
  if (pattern_duration < dt ) {
    setValues(0,0,0,0);
  }
  else if (pattern_duration < dt*2) {
    setValues(0,0,0,1);
  }
  else if (pattern_duration < dt*3) {
    setValues(0,0,1,0);
  }
  else if (pattern_duration < dt*4) {
    setValues(0,1,0,0);
  }
  else if (pattern_duration < dt*5) {
    setValues(1,0,0,0);
  }
  else {
    pattern_start_time = millis(); //repeat
  }
}

void pattern_cycle_bright(int dt) {
  pattern_duration = millis() - pattern_start_time;
  if (pattern_duration < dt ) {
    setValues(1,1,1,1);
  }
  else if (pattern_duration < dt*2) {
    setValues(1,0,1,1);
  }
  else if (pattern_duration < dt*3) {
    setValues(0,1,1,0);
  }
  else if (pattern_duration < dt*4) {
    setValues(1,1,0,1);
  }
  else if (pattern_duration < dt*5) {
    setValues(1,1,1,0);
  }
  else {
    pattern_start_time = millis(); //repeat
  }
}

void idle(int dt){
  pattern_duration = millis() - pattern_start_time;
  if (pattern_duration < dt) {
    setValues(1,1, 1,1);
  }
  else if (pattern_duration < dt*1.2) {
    setValues(1,1, 1,0);
  }
  else if (pattern_duration < dt*1.4) {
    setValues(1,1, 0,1);
  }
  else if (pattern_duration < dt*1.6) {
    setValues(1,0, 1,0);
  }
  else if (pattern_duration < dt*1.8) {
    setValues(0,1, 1,0);
  }
  else if (pattern_duration < dt*2) {
    setValues(0,0, 0,1);
  }
  else {
    setValues(0,0,0,0);
  }
}

int last_pattern = -1;

void timestep(){
  motion_duration = millis() - motion_start_time;
  
  int pattern = int(motion_duration / pattern_timeout) % NUM_PATTERNS;
  if (pattern != last_pattern) {
    Serial.print("Pattern ");
    Serial.println(pattern);
    last_pattern = pattern;
  }
  
  switch(pattern) {
    case 0:
      pattern_startup(default_dt);
      break;
    case 1:
      pattern_twinkle_bright(default_dt);
      break;
    case 2:
      pattern_twinkle_bright_repeat(default_dt);
      break;
    case 3:
      pattern_twinkle_dim(default_dt);
      break;
    case 4:
      pattern_twinkle_dim_simple(default_dt);
      break;
    case 5:
      pattern_cycle_dim(default_dt);
      break;
    case 6:
      pattern_simple_alternate_dim(default_dt);
      break;
    case 7:
      pattern_simple_alternate_dim(default_dt);
      break;
    case 8:
      pattern_cycle_bright(default_dt);
      break;
    default:
      pattern_twinkle_dim_simple(default_dt);
  }
}

void loop() {
  motion_prev = motion;
  motion = detect_motion();
  sigdown = motion_prev & !motion;
  sigup = !motion_prev & motion;
  
  if (active) {
    // If there is already motion OR no motion but still active (i.e. within timeout)
    if (motion || sigdown) {
      motion_timeout_start_time = millis();
    }
    timestep();
    if (millis() - motion_timeout_start_time >= motion_timeout) {
      active = false;
      pattern_start_time = millis(); //set start time for idle pattern
      Serial.println("timeout");
    }
  }
  else {
    idle(default_dt);
    if (motion) {
      active = true;
      Serial.println("active");
      if (sigup) {
        motion_start_time = millis(); //remember start time of motion
        pattern_start_time = millis(); //set start time for initial pattern
        Serial.println("Start Motion");
      }
    }
  }
}

